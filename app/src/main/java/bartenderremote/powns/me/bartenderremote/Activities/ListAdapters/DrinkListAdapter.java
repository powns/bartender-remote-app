package bartenderremote.powns.me.bartenderremote.Activities.ListAdapters;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import bartenderremote.powns.me.bartenderremote.Drink;
import bartenderremote.powns.me.bartenderremote.R;

public class DrinkListAdapter extends ArrayAdapter<Drink> {

    private boolean isEditView;

    public DrinkListAdapter(Context context, ArrayList<Drink> drinks, boolean isEditView) {
        super(context, 0, drinks);

        this.isEditView = isEditView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Drink drink = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            int targetLayout = isEditView ? R.layout.activity_drinks_manager_list_item : R.layout.activity_drinks_serving_list_item;
            convertView = LayoutInflater.from(getContext()).inflate(targetLayout, parent, false);
        }

        // Lookup view for data population
        TextView drinkName = isEditView ? (TextView) convertView.findViewById(R.id.drinkTitle) : (TextView) convertView.findViewById(R.id.drinkServingName);
        TextView drinkIngrList = (TextView) convertView.findViewById(R.id.drinkIngredients);

        // Populate the data into the template view using the data object
        drinkName.setText(drink.getDrinkName());
        if(isEditView){
            drinkIngrList.setText(drink.getIngredients().toString());
        }
        // Return the completed view to render on screen
        return convertView;
    }

}
