package bartenderremote.powns.me.bartenderremote.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import bartenderremote.powns.me.bartenderremote.Activities.ListAdapters.DrinkListAdapter;
import bartenderremote.powns.me.bartenderremote.Drink;
import bartenderremote.powns.me.bartenderremote.R;
import bartenderremote.powns.me.bartenderremote.Repository;

public class ServeDrinkActivity extends AppCompatActivity {

    private ListView drinksList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_serve_drink);

        ArrayList<Drink> allDrinksList = Repository.getAllDrinks();
        drinksList = findViewById(R.id.serverDrinks);
        DrinkListAdapter adapter = new DrinkListAdapter(this, allDrinksList, false);
        drinksList.setAdapter(adapter);

        drinksList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Drink drink = (Drink) parent.getItemAtPosition(position);
                System.out.println(drink);
            }
        });
    }
}
