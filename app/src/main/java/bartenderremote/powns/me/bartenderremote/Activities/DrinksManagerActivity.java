package bartenderremote.powns.me.bartenderremote.Activities;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import bartenderremote.powns.me.bartenderremote.Activities.ListAdapters.DrinkListAdapter;
import bartenderremote.powns.me.bartenderremote.Drink;
import bartenderremote.powns.me.bartenderremote.R;
import bartenderremote.powns.me.bartenderremote.Repository;

public class DrinksManagerActivity extends AppCompatActivity {

    private ListView drinksList;
    private FloatingActionButton addButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drinks_manager);

        addButton = findViewById(R.id.addButton);

        ArrayList<Drink> allDrinksList = Repository.getAllDrinks();
        drinksList = findViewById(R.id.drinksList);
        DrinkListAdapter adapter = new DrinkListAdapter(this, allDrinksList, true);
        drinksList.setAdapter(adapter);

        drinksList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Drink item = (Drink) parent.getItemAtPosition(position);
                System.out.println(item);
            }

        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DrinksManagerActivity.this, AddDrinkActivity.class));
            }
        });
    }
}
