package bartenderremote.powns.me.bartenderremote;

import java.util.HashMap;

public class Drink {
    private String drinkName;
    private HashMap<String, Integer> ingredients;

    public Drink(String drinkName, HashMap<String, Integer> ingredients) {
        this.drinkName = drinkName;
        this.ingredients = ingredients;
    }

    public String getDrinkName() {
        return drinkName;
    }

    public void setDrinkName(String drinkName) {
        this.drinkName = drinkName;
    }

    public HashMap<String, Integer> getIngredients() {
        return ingredients;
    }

    public void setIngredients(HashMap<String, Integer> ingredients) {
        this.ingredients = ingredients;
    }

    public void addIngredient(String ingredientName, int amountMilliLiters){
        ingredients.put(ingredientName, amountMilliLiters);
    }
}
