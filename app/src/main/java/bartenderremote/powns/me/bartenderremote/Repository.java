package bartenderremote.powns.me.bartenderremote;

import java.util.ArrayList;
import java.util.HashMap;

public class Repository {

    private static ArrayList<Drink> allDrinks = new ArrayList<>();

    public static void setupDrinksList(){
        Drink testDrink = new Drink("TestDrink", new HashMap<String, Integer>());

        testDrink.addIngredient("Coca Cola", 100);
        testDrink.addIngredient("Bacardi", 50);

        if(allDrinks.contains(testDrink)){
            return;
        }

        allDrinks.add(testDrink);
    }

    public static ArrayList<Drink> getAllDrinks(){
        return allDrinks;
    }

    public static void addDrink(Drink d){
        allDrinks.add(d);
    }
}
