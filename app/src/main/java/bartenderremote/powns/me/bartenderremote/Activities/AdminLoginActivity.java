package bartenderremote.powns.me.bartenderremote.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import bartenderremote.powns.me.bartenderremote.R;

public class AdminLoginActivity extends AppCompatActivity {

    private TextView name, pw;
    private Button loginBttn;
    private View.OnClickListener clickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_login);

        setupClickListener();
        setupUI();
    }

    private void setupClickListener(){
        clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if(name.getText().toString().equalsIgnoreCase("User") && pw.getText().toString().equalsIgnoreCase("test123")){
                    MainScreenActivity.loggedIn = true;
                    System.out.println("Logged in!");
                    startActivity(new Intent(AdminLoginActivity.this, DrinksManagerActivity.class));
                    //open new UI
                //}
            }
        };
    }

    private void setupUI(){
        name = findViewById(R.id.usernameTxt);
        pw = findViewById(R.id.pwText);

        loginBttn = findViewById(R.id.loginBttn);
        loginBttn.setOnClickListener(clickListener);
    }

}
