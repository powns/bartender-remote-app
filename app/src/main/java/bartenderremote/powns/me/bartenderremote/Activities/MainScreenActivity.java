package bartenderremote.powns.me.bartenderremote.Activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

import java.util.ArrayList;

import bartenderremote.powns.me.bartenderremote.R;
import bartenderremote.powns.me.bartenderremote.Repository;

public class MainScreenActivity extends AppCompatActivity {

    private ImageButton serveDrink, configureDrinks;
    private View.OnClickListener clickListener;
    private ArrayList<ImageButton> buttonList = new ArrayList<>();
    public  static boolean loggedIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);

        Repository.setupDrinksList();

        setupClickListener();
        setupUI();
    }

    private void setupClickListener(){
        clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v == configureDrinks){
                    startActivity(new Intent(MainScreenActivity.this, AdminLoginActivity.class));
                }
                else if (v == serveDrink){
                    startActivity(new Intent(MainScreenActivity.this, ServeDrinkActivity.class));
                }
            }
        };
    }

    private void setupUI(){
        buttonList.add(serveDrink = findViewById(R.id.serveDrink));
        buttonList.add(configureDrinks = findViewById(R.id.editDrinks));

        for(ImageButton imgButton : buttonList){
            imgButton.setOnClickListener(clickListener);
        }
    }
}
