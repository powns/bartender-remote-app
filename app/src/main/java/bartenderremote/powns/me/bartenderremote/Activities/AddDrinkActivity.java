package bartenderremote.powns.me.bartenderremote.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import bartenderremote.powns.me.bartenderremote.Drink;
import bartenderremote.powns.me.bartenderremote.R;
import bartenderremote.powns.me.bartenderremote.Repository;

public class AddDrinkActivity extends AppCompatActivity {

    private String[] availableIngredients = new String[]{"Bacardi", "Cola", "Vodka", "Whisky"};
    private Spinner spinner1, spinner2, spinner3, spinner4, spinner5, spinner6;
    private TextView drinkTitle, ingredient1, ingredient2, ingredient3, ingredient4, ingredient5, ingredient6;
    private Button submit;

    private ArrayList<Spinner> allSpinners;
    private ArrayList<TextView> allIngredientValues;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_drink);


        setupButton();
        setupTextBoxes();
        setupSpinners();
    }

    private void setupSpinners(){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, availableIngredients);

        allSpinners = new ArrayList<Spinner>();

        allSpinners.add(spinner1 = findViewById(R.id.spinner1));
        allSpinners.add(spinner2 = findViewById(R.id.spinner2));
        allSpinners.add(spinner3 = findViewById(R.id.spinner3));
        allSpinners.add(spinner4 = findViewById(R.id.spinner4));
        allSpinners.add(spinner5 = findViewById(R.id.spinner5));
        allSpinners.add(spinner6 = findViewById(R.id.spinner6));

        for(Spinner s : allSpinners){
            s.setAdapter(adapter);
        }
    }

    private void setupTextBoxes(){
        drinkTitle = findViewById(R.id.addDrinkTitle);

        allIngredientValues = new ArrayList<TextView>();

        allIngredientValues.add(ingredient1 = findViewById(R.id.addDrinkIngredientAmount1));
        allIngredientValues.add(ingredient2 = findViewById(R.id.addDrinkIngredientAmount2));
        allIngredientValues.add(ingredient3 = findViewById(R.id.addDrinkIngredientAmount3));
        allIngredientValues.add(ingredient4 = findViewById(R.id.addDrinkIngredientAmount4));
        allIngredientValues.add(ingredient5 = findViewById(R.id.addDrinkIngredientAmount5));
        allIngredientValues.add(ingredient6 = findViewById(R.id.addDrinkIngredientAmount6));
    }

    private void setupButton(){
        submit = findViewById(R.id.addDrinkButton);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Drink drink = buildDrinkFromRawData();
                Repository.addDrink(drink);
                System.out.println(drink);

                startActivity(new Intent(AddDrinkActivity.this, DrinksManagerActivity.class));
            }
        });
    }

    private Drink buildDrinkFromRawData(){
        String drinkName = drinkTitle.getText().toString();
        HashMap<String, Integer> ingredients = new HashMap<>();

        for(int i = 0; i < 6; i++){
            Spinner currentSpinner = allSpinners.get(i);
            TextView currentTextBox = allIngredientValues.get(i);

            if(!(ingredients.containsKey(currentSpinner.getSelectedItem().toString()))){
                ingredients.put(currentSpinner.getSelectedItem().toString(), Integer.parseInt(currentTextBox.getText().toString()));
            }
        }

        return new Drink(drinkName, ingredients);
    }
}
